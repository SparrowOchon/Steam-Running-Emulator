/**
* FILENAME :        donothing.c
*
* DESCRIPTION :
*        The following code base is meant to allow for time farming on steam.Simply compile this code
*        and replace the game executable with this. This will allow you to keep the game on for long periods  of
*        time while taking as little resources as possible.
*
* NOTES :
*
*	Show don't Sell License Version 1
*   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
*
*       Copyright Network Silence. 2018.  All rights reserved.
*
*
*
* AUTHOR :   Network Silence        START DATE :    13 Dec 2018
*
*/
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

int main()
{
	const int linuxSleepInSeconds = 86400; // 24 Hours
	const int windowsSleepInMilliSeconds = 86400000; // 24 hours
#ifdef _WIN32
	HWND curWindow = GetConsoleWindow();
	ShowWindow(curWindow, SW_HIDE);
	while (1) {
		Sleep(windowsSleepInMilliSeconds);
	}
#else
	while (1) {
		sleep(linuxSleepInSeconds);
	}
#endif
	return 0;
}
