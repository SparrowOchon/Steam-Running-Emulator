cmake_minimum_required(VERSION 3.12)
project(do_nothing)

set(CMAKE_CXX_STANDARD 14)

add_executable(do_nothing donothing.c)